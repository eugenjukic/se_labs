 Lab 3

## Configure python virtual environment 

https://realpython.com/python-virtual-environments-a-primer/

## Django tutorials

https://docs.djangoproject.com/en/3.1/intro/tutorial01/

For details see: [Official Python tutorial](https://docs.python.org/3/tutorial/index.html)

History:

    1  git clone https://gitlab.com/eugenjukic/se_labs.git
    2  ls
    3  cd se_labs
    4  git config user.name "Eugen Jukić"
    5  git config user.email "eugenjukic13@gmail.com"
    6  git remote add upstream https://gitlab.com/levara/se_labs
    7  git fetch upstream master
    8  git merge upstream/master
    9  git push origin master
   10  cd lab3/
   11  ls
   12  mkdir venv
   13  cd venv
   14  python -m venv env
   15  ls
   16  which python3
   17  ls env/
   18  ls env/Scripts/
   19  source env/Scripts/activate
   20  which python
   21  deactivate
   22  which python
   23  source env/Scripts/activate
   24  which python
   25  pip install django
   26  history


