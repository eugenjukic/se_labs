from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Image, Comment, Vote, Like, User
from .forms import ImageForm, CommentForm
# Create your views here.

def index(request):
    images = Image.objects.order_by('-pub_date')
    
    votes = [image.vote_by(request.user) for image in images ]
    context = { 'images_with_votes': zip(images,votes)}
    return render(request, 'app/index.html', context)

def detail(request, image_id):
    image = get_object_or_404(Image,pk=image_id)
    context = {'image': image, 
               'vote': image.vote_by(request.user),
               'comments': image.comment_set.all(),
               'form' : CommentForm(),
              }
    return render(request, 'app/detail.html', context)

def profile(request, user_id):
    otherUser = User.objects.filter(id = user_id).first()
    images = Image.objects.filter(user_id = user_id)
    comments = Comment.objects.filter(user_id = user_id)
    votes = [image.vote_by(request.user) for image in images ]
    context = {'images_with_votes' :zip(images,votes),
               'comments': comments,
               'otherUser': otherUser,}
    return render(request, 'app/profile.html', context)
    

def create_image(request):
    if request.method == 'POST' and request.user.is_authenticated:
        form= ImageForm(request.POST)
        if form.is_valid():
            image = form.save(commit = False)
            image.user = request.user
            image.save()
            return HttpResponseRedirect(reverse('app:detail', args=(image.id,)))
    else:
        form = ImageForm()
    context = { 'form' :form, 'action': 'create' }
    return render(request, 'app/create_image.html', context)

def update_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if request.method == 'POST' and image.user == request.user or request.method == 'POST' and request.user.is_superuser:
        form = ImageForm(request.POST, instance = image)
        if form.is_valid():
            saved_image = form.save()
            return HttpResponseRedirect(reverse('app:detail', args=(saved_image.id,)))
    else:
        form = ImageForm(instance=image)
    context = { 'form': form, 'action':'update'}
    return render(request, 'app/create_image.html', context)

def delete_image(request, image_id):
    image = get_object_or_404(Image, pk=image_id)
    if request.method == 'POST' and image.user == request.user or request.method == 'POST' and request.user.is_superuser:
        image.delete()
    return HttpResponseRedirect(reverse('app:index'))

def comment(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        image = get_object_or_404(Image, pk=image_id)
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit = False)
            comment.image = image
            comment.user = request.user
            comment.save()
            return HttpResponseRedirect(reverse('app:detail',
            args=(comment.image.id,)))
        else:
            return render(request, 'app/detail.html', {
                'image': image,
                'comments' : image.comment_set.all(),
                'form': form,
            })
    else:
        return HttpResponseRedirect(reverse('app:detail', args=(
            image_id,
        )))
def approve(request, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    if request.method == 'POST' and request.user.is_superuser:         
        comment.approved = True
        comment.save()
        return HttpResponseRedirect(reverse('app:detail', args=(comment.image.id,)))

def vote(request, image_id, upvote):
    image = get_object_or_404(Image, pk=image_id)
    vote = Vote.objects.filter(user=request.user, image=image).first()
    if vote:
        if vote.upvote== upvote:
            vote.delete()
            return None
        else:
            vote.upvote = upvote
    else:
        vote = Vote(user=request.user, image=image, upvote=upvote)
    try:
        vote.full_clean()
        vote.save()
    except Exception as e:
        print(e)
        return None
    else:
        return vote

def upvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request, image_id, True)
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def downvote(request, image_id):
    if request.method == 'POST' and request.user.is_authenticated:
        vote(request, image_id, False)
    return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))

def like(request, comment_id, liked):
    
    comment = get_object_or_404(Comment, pk=comment_id)
    like = Like.objects.filter(user=request.user, comment = comment).first()
    if like:
        if like.liked == liked:
            like.delete()
            return None
        else:
            like.liked = liked
    else: 
        like = Like(user=request.user, comment = comment, liked = liked)
    try:
        like.full_clean()
        like.save()
    except Exception as e:
        print(e)
        return None
    else:
        return like
    

def liked(request, comment_id, image_id):
     if request.method == 'POST' and request.user.is_authenticated:
         like(request, comment_id, True)
     return HttpResponseRedirect(reverse('app:detail', args=(image_id,)))
